const ajoutItemToForm = () => {
    let div = document.createElement("div");
    let input = document.createElement("input");
    let button = document.createElement("button");

    let newName = "item-" + document.getElementsByClassName("inputItem").length;
    let newPlaceholder = "item " + document.getElementsByClassName("inputItem").length;

    input.setAttribute("type", "text");
    input.setAttribute("name", newName);
    input.setAttribute("placeholder", newPlaceholder);
    input.classList.add("inputItem");

    button.innerText = "supprimer item";
    button.onclick = function () {
        let i = 0;
        div.remove();
        for(let element of document.getElementsByClassName("inputItem")){
            element.setAttribute("name", "item-" + i);
            element.setAttribute("placeholder", "item " + i);
            i++;
        };
    };

    div.appendChild(input);
    div.appendChild(button);
    document.getElementById("formTopList").appendChild(div);
}

const saveNewList = () => {
    let listName = document.getElementById("listName").value;
    let topLists = JSON.parse(window.localStorage.getItem("topLists"));
    if(topLists.indexOf(listName) === -1) {
        topLists.push(listName);
        window.localStorage.setItem("topLists", JSON.stringify(topLists));
    }
    let top = [];
    for (let i = 0; i<document.getElementsByClassName("inputItem").length; i++) {
        top.push(document.getElementsByClassName("inputItem")[i].value);
    }
    window.localStorage.setItem(listName, JSON.stringify(top));
    resetFormAjoutTopList();
    rerender();
}

const rerender = () => {
    document.getElementById("page2").classList.add("hidden");
    document.getElementById("page1").classList.remove("hidden");
    document.getElementById("listContainer").innerHTML = "";
    render();
}

const render = () => {
    let topLists = JSON.parse(window.localStorage.getItem("topLists"));
    for(let i = 0; i < topLists.length; i++) {
        let div = document.createElement("div");
        div.classList.add("list-item");

        let item = document.createElement("span");
        item.classList.add("item");
        item.innerText = topLists[i];
        item.onclick = () => {
            showList(topLists[i]);
        }
        div.appendChild(item);

        let button = document.createElement("button");
        button.innerText = "SUPPRIMER";
        button.onclick = () => {
            window.localStorage.removeItem(topLists[i]);
            for( let y = 0; y < topLists.length; y++){ 
                if ( topLists[y] === topLists[i]) { 
                    topLists.splice(y, 1); 
                }
            }
            window.localStorage.setItem("topLists", JSON.stringify(topLists));
            rerender();
        }
        div.appendChild(button);

        document.getElementById("listContainer").appendChild(div);
    }
}

const showList = (_nomList) => {
    document.getElementById("page1").classList.add("hidden");
    document.getElementById("page2").classList.remove("hidden");
    document.getElementById("listName").value = _nomList;
    let top = JSON.parse(window.localStorage.getItem(_nomList));
    for (let i = 0; i< top.length; i++) {
        ajoutItemToForm();
        document.getElementsByClassName("inputItem")[i].value = top[i];
    }
}