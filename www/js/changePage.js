document.getElementById("ajoutList").onclick = function() {
    document.getElementById("page1").classList.add("hidden");
    document.getElementById("page2").classList.remove("hidden");
}

document.getElementById("flecheRetour").onclick = function() {
    resetFormAjoutTopList();
    document.getElementById("page2").classList.add("hidden");
    document.getElementById("page1").classList.remove("hidden");
}

const resetFormAjoutTopList = () => {
    document.getElementById("formTopList").innerHTML = "";
    document.getElementById("listName").value = "";
}