window.addEventListener("batterystatus", onBatteryStatus, false);

function onBatteryStatus(status) {
    if(status.isPlugged) {
        document.getElementById("battery").classList.add("bg-success");
    } else {
        document.getElementById("battery").classList.remove("bg-success");
    }
    document.getElementById("battery").innerHTML = status.level;
    document.getElementById("battery").setAttribute("aria-valuenow", status.level);
    document.getElementById("battery").setAttribute("style", "width: " + status.level + "%");
}